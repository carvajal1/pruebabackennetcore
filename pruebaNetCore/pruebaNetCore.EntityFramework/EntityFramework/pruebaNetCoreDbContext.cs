﻿using Microsoft.EntityFrameworkCore;
using pruebaNetCore.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Text;

namespace pruebaNetCore.EntityFramework.EntityFramework
{
    
    public class pruebaNetCoreDbContext: DbContext
    {

        private readonly string _connectionString;

        public pruebaNetCoreDbContext()
        {
            _connectionString = "Server=DESKTOP-BF4DGG5\\SQLEXPRESS; Database=prbcarbajalDb; Trusted_Connection=True;";
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<TipoDocumentoEntity> tipoDocumentoEntities { get; set; }
        public DbSet<UsuarioEntity> usuarioEntities { get; set; }


    }
}
