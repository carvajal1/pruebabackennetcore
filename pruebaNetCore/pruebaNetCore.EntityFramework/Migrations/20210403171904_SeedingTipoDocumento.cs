﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace pruebaNetCore.EntityFramework.Migrations
{
    public partial class SeedingTipoDocumento : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("tipoDocumentoEntities", new string[] { "Id", "codigo", "detalle","CreationTime", "IsDeleted" }, new object[] {1, "CC", "Cedula ciudadania","2021-04-03 08:00:00",false  });
            migrationBuilder.InsertData("tipoDocumentoEntities", new string[] { "Id", "codigo", "detalle","CreationTime", "IsDeleted" }, new object[] { 2, "RC", "Registro Civil", "2021-04-03 08:00:00", false });
            migrationBuilder.InsertData("tipoDocumentoEntities", new string[] { "Id", "codigo", "detalle", "CreationTime", "IsDeleted" }, new object[] { 3, "TI", "Tarjeta de Identidad", "2021-04-03 08:00:00", false });
            migrationBuilder.InsertData("tipoDocumentoEntities", new string[] { "Id", "codigo", "detalle", "CreationTime", "IsDeleted" }, new object[] { 4, "CE", "Cedula Extranjeria", "2021-04-03 08:00:00", false });
            migrationBuilder.InsertData("tipoDocumentoEntities", new string[] { "Id", "codigo", "detalle", "CreationTime", "IsDeleted" }, new object[] { 5, "PA", "Pasaporte", "2021-04-03 08:00:00", false });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
