﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace pruebaNetCore.EntityFramework.Migrations
{
    public partial class SeedingUserInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("usuarioEntities", new string[] { 
                "Id", 
                "nombre", 
                "IdTipoIdentificacion",
                "numeroIdentificacion",
                "contrasena",
                "email",
                "CreationTime", 
                "IsDeleted" }, 
                new object[] {
                    1, 
                    "Admin",
                    1,
                    "88032255102",
                    "AQAAAAEAACcQAAAAEFICCoPrkmisbrnk970acISxck7GKl0VcT9gXIWT7z1tkNxzQ3hFnu0uDGxN10/n8g==",
                    "elkinduranm@gmail.com",
                    "2021-04-03 08:00:00", false });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
