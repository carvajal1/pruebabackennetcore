﻿using pruebaNetCore.Application.TipoDocumento.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaNetCore.Application.TipoDocumento
{
    public interface ITipoDocumentoAppService
    {
        Task<List<TipoDocumentoDto>> getTipoDocumento();
    }
}
