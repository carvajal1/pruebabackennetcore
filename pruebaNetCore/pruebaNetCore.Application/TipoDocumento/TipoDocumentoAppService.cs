﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using pruebaNetCore.Application.TipoDocumento.DTO;
using pruebaNetCore.Core.Entidades;
using pruebaNetCore.EntityFramework.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaNetCore.Application.TipoDocumento
{
    public class TipoDocumentoAppService : ITipoDocumentoAppService
    {
        private readonly pruebaNetCoreDbContext _dbcontext;
        private readonly IMapper _mapper;

        public TipoDocumentoAppService(IMapper mapper)
        {
            _mapper = mapper;
            _dbcontext = new pruebaNetCoreDbContext();
        }
        public async Task<List<TipoDocumentoDto>> getTipoDocumento()
        {
            try
            {
                var entity = await _dbcontext.tipoDocumentoEntities.ToListAsync();
                return _mapper.Map<List<TipoDocumentoEntity>,List<TipoDocumentoDto>>(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
