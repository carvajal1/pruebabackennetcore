﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaNetCore.Application.TipoDocumento.DTO
{
    public class TipoOutPut
    {
        public string status { get; set; }
        public string error { get; set; }
        public List<TipoDocumentoDto> tipo { get; set; }
        public string detalle { get; set; }
    }
}
