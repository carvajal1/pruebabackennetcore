﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaNetCore.Application.TipoDocumento.DTO
{
    public class TipoDocumentoDto
    {
        public string codigo { get; set; }
        public string detalle { get; set; }
    }
}
