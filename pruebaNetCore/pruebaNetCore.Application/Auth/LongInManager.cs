﻿using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using pruebaNetCore.Application.Auth.DTO;
using pruebaNetCore.Core.Entidades;
using pruebaNetCore.EntityFramework.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace pruebaNetCore.Application.Auth
{
    public class LongInManager : ILongInManager
    {
        private readonly pruebaNetCoreDbContext _dbcontext;

        public LongInManager()
        {
            _dbcontext = new pruebaNetCoreDbContext();
        }

        

        public async Task<LoginModel> LoginAsync(string documento, string pass)
        {

            try
            {
                var p = Encryptar.GetHash256(pass);
                var dato = await _dbcontext.usuarioEntities.FirstOrDefaultAsync(x => x.numeroIdentificacion == documento && x.contrasena == Encryptar.GetHash256(pass));
                LoginModel login = new LoginModel();
                if (dato != null)
                {
                    login.nombre = dato.nombre;
                    login.apellido = dato.apellido;
                    login.Id = dato.Id;
                }
                
               
                return login;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


        }
    }
}
