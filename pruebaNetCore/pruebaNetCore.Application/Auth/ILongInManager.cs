﻿using pruebaNetCore.Application.Auth.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaNetCore.Application.Auth
{
    public interface ILongInManager
    {
        Task<LoginModel> LoginAsync(string documento, string pass);
    }
}
