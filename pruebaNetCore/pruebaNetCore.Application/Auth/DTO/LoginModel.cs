﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace pruebaNetCore.Application.Auth.DTO
{
    public class LoginModel
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
        public long Id { get; set; }
        public ClaimsIdentity Identity { get; set; }
    }
}
