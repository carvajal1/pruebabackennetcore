﻿using AutoMapper;
using pruebaNetCore.Application.TipoDocumento.DTO;
using pruebaNetCore.Application.Usuario.DTO;
using pruebaNetCore.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaNetCore.Application
{
    public class AutoProfile : Profile
    {
        public AutoProfile()
        {
            CreateMap<UsuarioEntity, UsuarioOutPutDto>()
              .ForMember(dest => dest.nombre, act => act.MapFrom(src => src.nombre))
              .ForMember(dest => dest.apellido, act => act.MapFrom(src => src.apellido))
              .ForMember(dest => dest.email, act => act.MapFrom(src => src.email))
              .ForMember(dest => dest.numeroIdentificacion, act => act.MapFrom(src => src.numeroIdentificacion))
              .ForMember(dest => dest.tipoIdentificacion, act => act.MapFrom(src => src.tipoIdentificacion.codigo))
              .ForMember(dest => dest.id, act => act.MapFrom(src => src.Id));
            CreateMap<TipoDocumentoEntity,TipoDocumentoDto>()
                .ForMember(dest => dest.codigo, act => act.MapFrom(src => src.codigo))
                .ForMember(dest => dest.detalle, act => act.MapFrom(src => src.detalle));
        }
    }
}
