﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaNetCore.Application.Usuario.DTO
{
    public class UsuarioInputDto
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string tipoIdentificacion { get; set; }
        public string numeroIdentificacion { get; set; }
        public string contrasena { get; set; }
        public string email { get; set; }
    }
}
