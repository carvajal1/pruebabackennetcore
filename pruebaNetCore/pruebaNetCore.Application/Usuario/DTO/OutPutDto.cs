﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaNetCore.Application.Usuario.DTO
{
    public class OutPutDto
    {
        public string status { get; set; }
        public string error { get; set; }
        public UsuarioOutPutDto usuario { get; set; }
        public string detalle { get; set; }
    }
}
