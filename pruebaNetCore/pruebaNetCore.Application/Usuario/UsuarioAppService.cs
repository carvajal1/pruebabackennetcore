﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using pruebaNetCore.Application.Usuario.DTO;
using pruebaNetCore.Core.Entidades;
using pruebaNetCore.EntityFramework.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebaNetCore.Application.Usuario
{
    public class UsuarioAppService : IUsuarioAppService
    {
        private readonly pruebaNetCoreDbContext _dbcontext;
        private readonly IMapper _mapper;
        public UsuarioAppService(IMapper mapper)
        {
            _mapper = mapper;
            _dbcontext = new pruebaNetCoreDbContext();
        }

        public async Task<int> ActualizarUsuario(UsuarioInputDto input)
        {
            try
            {
                
                var entity = _dbcontext.usuarioEntities.Where(x => x.numeroIdentificacion == input.numeroIdentificacion && x.IsDeleted==false).FirstOrDefault();
                
                entity.apellido = input.apellido;
                entity.LastModificationTime = new DateTime();
                entity.email = input.email;
                entity.nombre = input.nombre;
                
                 _dbcontext.usuarioEntities.Update(entity);
                return await _dbcontext.SaveChangesAsync();
               
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public async Task<UsuarioOutPutDto> ConsultarUsuario(string numeroDocumento)
        {
            try
            {
               
                var entity = await _dbcontext.usuarioEntities
                    .Include(x=>x.tipoIdentificacion)
                    .FirstOrDefaultAsync(x => x.numeroIdentificacion == numeroDocumento && x.IsDeleted==false);
                if (entity == null) return null;
                return _mapper.Map<UsuarioOutPutDto>(entity);
                //return new UsuarioOutPutDto
                //{
                //    apellido=entity.apellido,
                //    email=entity.email,
                //    id= (int)entity.Id,
                //    nombre=entity.nombre,
                //    numeroIdentificacion=entity.numeroIdentificacion,
                //    tipoIdentificacion= entity.tipoIdentificacion.codigo
                //};
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public async Task<int> EliminarUsuario(string numeroDocumento)
        {
            try
            {
                
                var entity=  await _dbcontext.usuarioEntities
                     .Include(x => x.tipoIdentificacion)
                     .FirstOrDefaultAsync(x => x.numeroIdentificacion == numeroDocumento && x.IsDeleted==false);
              
                entity.IsDeleted = true;
                entity.DeletionTime = new DateTime();
                 _dbcontext.usuarioEntities.Update(entity);
               return await _dbcontext.SaveChangesAsync();
               
            }
            catch (Exception e)
            {

                throw;
            }
        }

       

        public async Task<int> RegistrarUsuario(UsuarioInputDto input)
        {
            try
            {
                bool respuesta = false;
                var tipo = _dbcontext.tipoDocumentoEntities.Where(x => x.codigo == input.tipoIdentificacion).FirstOrDefault();
                UsuarioEntity entity = new UsuarioEntity
                {
                    apellido = input.apellido,
                    contrasena = Encryptar.GetHash256(input.contrasena),
                    CreationTime = new DateTime(),
                    email = input.email,
                    IsDeleted = false,
                    nombre = input.nombre,
                    numeroIdentificacion = input.numeroIdentificacion,
                    tipoIdentificacion = tipo
                };
               
                await _dbcontext.usuarioEntities.AddAsync(entity);
                return await _dbcontext.SaveChangesAsync();
               
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
