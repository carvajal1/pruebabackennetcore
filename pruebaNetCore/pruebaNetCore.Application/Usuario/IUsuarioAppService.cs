﻿using pruebaNetCore.Application.Usuario.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaNetCore.Application.Usuario
{
    public interface IUsuarioAppService
    {
        Task<int> RegistrarUsuario(UsuarioInputDto input);
        Task<int> ActualizarUsuario(UsuarioInputDto input);
        Task<UsuarioOutPutDto> ConsultarUsuario(string numeroDocumento);
        Task<int> EliminarUsuario(string numeroDocumento);
    }
}
