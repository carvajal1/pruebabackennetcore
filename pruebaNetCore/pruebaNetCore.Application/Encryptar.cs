﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace pruebaNetCore.Application
{
    public class Encryptar
    {
        public static string GetHash256(string input)
        {
            SHA256 sha = SHA256Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha.ComputeHash(encoding.GetBytes(input));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
}
