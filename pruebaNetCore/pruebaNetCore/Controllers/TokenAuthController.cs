﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Runtime.Security;
using pruebaNetCore.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using pruebaNetCore.Application;
using pruebaNetCore.Application.Auth;
using pruebaNetCore.Application.Auth.DTO;
using pruebaNetCore.EntityFramework.EntityFramework;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaNetCore.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class TokenAuthController : ControllerBase
    {
        
        private readonly ILongInManager _longInManager;
        private readonly IConfiguration _appseting = BuildConfiguration();

        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                             .SetBasePath(Directory.GetCurrentDirectory())
                             .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                             .AddJsonFile("appsettings.Production.json", optional: true, reloadOnChange: true);
            return builder.Build();

        }

        public TokenAuthController(
            ILongInManager longInManager
           )
        {
            
            _longInManager = longInManager;
           
        }

        [HttpPost]
        public async Task<AuthenticateResultModel> Authenticate([FromForm] AuthenticateModel model)
        {
            var aut = new AuthenticateResultModel();
            try
            {
                var loginResult = await GetLoginResultAsync(
               model.identificacion,
               model.Password
           );

                if (loginResult.nombre != null)
                {
                    var accessToken = generateJwtToken(loginResult);

                    aut.AccessToken = accessToken;
                    aut.EncryptedAccessToken = GetEncryptedAccessToken(accessToken);
                    aut.ExpireInSeconds = 86400;
                    aut.UserId = loginResult.Id;

                }
                else
                {
                    aut.error = "Error de autenticación en el Usuario o Contraseña";
                }
            }
            catch (Exception e)
            {

                aut.error = "Error fatal: "+e.Message;
            }

            return aut;
        }


        private async Task<LoginModel> GetLoginResultAsync(string usernameOrEmailAddress, string password)
        {
            return await _longInManager.LoginAsync(usernameOrEmailAddress, password);

           
        }

       
        private string generateJwtToken(LoginModel loginResult)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_appseting.GetValue<string>("Authentication:JwtBearer:SecurityKey"));
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", loginResult.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private string GetEncryptedAccessToken(string accessToken)
        {
            return SimpleStringCipher.Instance.Encrypt(accessToken, AppConsts.DefaultPassPhrase);
        }
    }
}