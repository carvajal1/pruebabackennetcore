﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using pruebaNetCore.Application.Usuario;
using pruebaNetCore.Application.Usuario.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioAppService _serviceUsuario;
        public UsuarioController(IUsuarioAppService serviceUsuario)
        {
            _serviceUsuario = serviceUsuario;
        }
        // GET: api/<UsuarioController>
        

        // GET api/<UsuarioController>/5
        [HttpGet("{documento}")]
        public async Task<OutPutDto> Get(string documento)
        {
            OutPutDto outPut = new OutPutDto();
            try
            {
                var dto = await _serviceUsuario.ConsultarUsuario(documento);
               
                outPut.error = null;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = dto;
                outPut.detalle = "Consulta Exitosa";
            }
            catch (Exception e)
            {
                outPut.error = e.Message;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = null;
                outPut.detalle = "Consulta Fallida";

            }
            return outPut;
        }

        // POST api/<UsuarioController>
        [HttpPost]
        public async Task<OutPutDto> Post([FromForm] UsuarioInputDto input)
        {
            OutPutDto outPut = new OutPutDto();
            try
            {
                await _serviceUsuario.RegistrarUsuario(input);
                var dto = await _serviceUsuario.ConsultarUsuario(input.numeroIdentificacion);

                outPut.error = null;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = dto;
                outPut.detalle = "Regidtro de usuario Exitosa";
            }
            catch (Exception e)
            {
                outPut.error = e.Message;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = null;
                outPut.detalle = "Regidtro de usuario Fsallida";
            }
            return outPut;
        }
        // PUT api/<UsuarioController>/5
        [HttpPut]
        public async Task<OutPutDto> Put([FromForm] UsuarioInputDto input)
        {
            OutPutDto outPut = new OutPutDto();
            try
            {
               await _serviceUsuario.ActualizarUsuario(input);
                var dto = await _serviceUsuario.ConsultarUsuario(input.numeroIdentificacion);

                outPut.error = null;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = dto;
                outPut.detalle = "Actualización de usuario Exitosa";
            }
            catch (Exception e)
            {
                outPut.error = e.Message;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = null;
                outPut.detalle = "Actualización de usuario Fallida";
            }
            return outPut;
        }

        // DELETE api/<UsuarioController>/5
        [HttpDelete("{documento}")]
        public async Task<OutPutDto> Delete(string documento)
        {

            OutPutDto outPut = new OutPutDto();
            try
            {
                var dto = await _serviceUsuario.ConsultarUsuario(documento);

                outPut.error = null;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = dto;
                await _serviceUsuario.EliminarUsuario(documento);
                outPut.detalle = "Usuario Eliminado";

            }
            catch (Exception e)
            {
                outPut.error = e.Message;
                outPut.status = HttpContext.Response.StatusCode.ToString();
                outPut.usuario = null;
                outPut.detalle = "Error al Eliminar Usuario";
            }
            return outPut;
        }
    }
}
