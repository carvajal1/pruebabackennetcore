﻿using Microsoft.AspNetCore.Mvc;
using pruebaNetCore.Application.TipoDocumento;
using pruebaNetCore.Application.TipoDocumento.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tiposDocumentosController : ControllerBase
    {
        private readonly ITipoDocumentoAppService _service;
        public tiposDocumentosController(ITipoDocumentoAppService service)
        {
            _service = service;
        }
        // GET: api/<tiposDocumentosController>
        [HttpGet]
        public async Task<TipoOutPut> Get()
        {
            TipoOutPut resp = new TipoOutPut();
            try
            {
                resp.tipo=await _service.getTipoDocumento();
                resp.status = HttpContext.Response.StatusCode.ToString();
                resp.detalle = "Consulta exitosa" ;

            }
            catch (Exception e)
            {
                resp.status = HttpContext.Response.StatusCode.ToString();
                resp.error = e.Message;
                resp.detalle = "Error Fatal:" + e.ToString();
            }
            return resp;
        }

       
    }
}
