﻿using Microsoft.Extensions.DependencyInjection;
using pruebaNetCore.Application.Auth;
using pruebaNetCore.Application.TipoDocumento;
using pruebaNetCore.Application.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pruebaNetCore.MildWare
{
    public static class IoC
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            services.AddSingleton<IUsuarioAppService, UsuarioAppService>();
            services.AddSingleton< ILongInManager ,LongInManager>();
            services.AddSingleton<ITipoDocumentoAppService, TipoDocumentoAppService>();
            return services;
        }
    }
}
