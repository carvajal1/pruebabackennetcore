﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaNetCore.Core.Entidades
{
    public class TipoDocumentoEntity: FullAuditedEntity<long>
    {
        public string codigo { get; set; }
        public string detalle { get; set; }
    }
}
