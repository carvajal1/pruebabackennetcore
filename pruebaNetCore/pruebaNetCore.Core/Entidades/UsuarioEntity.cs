﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace pruebaNetCore.Core.Entidades
{
    public class UsuarioEntity: FullAuditedEntity<long>
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
        [ForeignKey("IdTipoIdentificacion")]
        public TipoDocumentoEntity tipoIdentificacion { get; set; }
        public long IdTipoIdentificacion { get; set; }
        public string numeroIdentificacion { get; set; }
        public string contrasena { get; set; }
        public string email { get; set; }
    }
}
